# coding: utf-8
import boto3
import botocore

session = boto3.Session(profile_name = "aws_iamuser" )
s3 = session.resource('s3')

def delete():
    delete_bucket = '<your bucket name>'
    
    try:
            delete_file = raw_input("\nEnter the file name you want to delete :")
            s3.Object(delete_bucket,delete_file).load()
    except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                print("File not found")
                exit()

    s3.Object(delete_bucket,delete_file).delete()
    print("\nyour file deleted successfully...!!")
    exit()

def upload():       
    
    bucket_name = raw_input("\nEnter the Bucket name where you want to upload an object (for eg. mybucket):")

    directory_name = raw_input("Enter the directory name you want to store in S3 Bucket (for eg. mydirectory) :")

    key = raw_input("Enter the Filename :")

    bucket = s3.Bucket(bucket_name)
    slash = "/"
    data = open(key, 'rb')

    image_save = raw_input("Enter the filename you want to store in S3 Bucket (for eg. myfile.png):")

    # NOTE : Put file_type= 'image/svg+xml' for svg files
    # NOTE : Put file_type= 'image/png' for png files
    # NOTE : Put file_type= 'image/jpeg' for jpeg or jpg files


    file_type = raw_input("Enter the File type of your image (for eg. image/png): ")

    result = bucket.put_object(Key=directory_name+image_save, Body=data, ContentType=file_type)

    print("\n\nFile Successfully Uploaded.....!!!\n\n")

    base_url = 'https://s3.ap-south-1.amazonaws.com/'
    object_url = base_url+bucket_name+slash+directory_name+slash+image_save

    print("Your File Url : " +object_url)
    print("\n\n")


print("1. For deleting the File")
print("2. For uploading the File")

def asking_function():
    input = raw_input("Enter the Option :")
    if input == "1":
        delete()
        exit()
    if input == "2":
        upload()
        exit()

asking_function()
